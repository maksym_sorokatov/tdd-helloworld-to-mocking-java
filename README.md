# TDD HelloWorld to Mocking 
**Estimated reading time**: 15 minutes 

## Story Outline
Dev/Engineering Managers can be huge allies when TDD is rolled out in an organization. The more they understand the practice, the better advocates they will be.
Teaching TDD to developers is one thing. Teaching TDD to Managers is another as they don't have patience to sit in a training unless they are skilled developers, and even then they
may not stop answering email during the training.  :-)

This short demonstration is an activity a developer can take this manager through in 30 minutes to an hour (whether the manager can write code or not) to understand the following concepts:
- How to do red, green, refactor
- How to write a test first
- Why is mocking necessary
- How to mock

This story takes us from "zero code" to TDD HelloWorld, and then by adding the requirement of posting the time at the end of HelloWorld, enters the world of needing a mock object.
There is a *bonus* section to illustrate the amount of complexity of handling a "noon" requirement. 
This bonus section is likely too long for a 30 minute tutorial to a manager 
but will show the complexity of dealing with edge cases by simply implementing the following: 
have the clock return a message about "noon" when it's after 11:30 and before 12:30.
 
## Story Organization
**Story Branch**: master
> `git checkout master` 

**Practical task tag for self-study**: task
> `git checkout task` 

Tags: #TDD, #mocking
